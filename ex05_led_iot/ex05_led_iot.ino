#include <ESP8266WiFi.h>

WiFiServer server(80);
const char* ssid     = "iptime22";
const char* password = "totopass5047";

const int led2 = D7;
const int led1 = D6;
String led2State = "off";
String led1State = "off";

// Variable to store the HTTP request
String header;  

unsigned long currentTime = millis();
unsigned long previousTime = 0; 
const long timeoutTime = 2000; // 2000ms = 2s

void setup() {
  Serial.begin(115200);
  pinMode(led2, OUTPUT);
  pinMode(led1, OUTPUT);
  digitalWrite(led2, LOW);
  digitalWrite(led1, LOW);

  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop(){
  WiFiClient client = server.available();

  if (client) {                          
    Serial.println("New Client.");       
    String currentLine = "";    
    currentTime = millis();
    previousTime = currentTime;
    while (client.connected() && currentTime - previousTime <= timeoutTime) { 
      currentTime = millis();         
      if (client.available()) {
        char c = client.read();
        Serial.write(c);       
        header += c;
        if (c == '\n') { 
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /2/on") >= 0) {
              Serial.println("LED2 on");
              led2State = "on";
              digitalWrite(led2, HIGH);
            } else if (header.indexOf("GET /2/off") >= 0) {
              Serial.println("LED2 off");
              led2State = "off";
              digitalWrite(led2, LOW);
            } else if (header.indexOf("GET /1/on") >= 0) {
              Serial.println("LED1 on");
              led1State = "on";
              digitalWrite(led1, HIGH);
            } else if (header.indexOf("GET /1/off") >= 0) {
              Serial.println("LED1 off");
              led1State = "off";
              digitalWrite(led1, LOW);
            }
            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #FF2211;}</style></head>");
            
            client.println("<body><h1>IoT Web Server</h1>");
            
            client.println("<p>LED2 - State : " + led2State + "</p>");
            if (led2State=="off") {
              client.println("<p><a href=\"/2/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/2/off\"><button class=\"button button2\">OFF</button></a></p>");
            } 
               
            client.println("<p>LED1 - State: " + led1State + "</p>");
            if (led1State=="off") {
              client.println("<p><a href=\"/1/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/1/off\"><button class=\"button button2\">OFF</button></a></p>");
            }
            client.println("</body></html>");
            
            client.println();
            break;
          } else {
            currentLine = "";
          }
        } else if (c != '\r') {  
          currentLine += c;    
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
